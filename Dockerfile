FROM node:8.12.0-alpine

WORKDIR /usr/src/app
COPY . .
EXPOSE 9000
CMD [ "npm", "run", "start:monitor" ]